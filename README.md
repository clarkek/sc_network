![](http://i.imgur.com/xeKD93p.png)a
# `$ raspap-webgui` [![Release 1.3.1](https://img.shields.io/badge/Release-1.3.1-green.svg)](https://github.com/billz/raspap-webgui/releases) [![Awesome](https://awesome.re/badge.svg)](https://github.com/thibmaek/awesome-raspberry-pi)
A simple, responsive web interface to control network connectivity wifi, hostapd and related services on the Smart Connector Hub.

This project is based on [**raspap-webgui**](

This project was inspired by a [**blog post**](https://github.com/billz/raspap-webgui/blob/master/README.md) by Bimmerman. That project has evolved to include greater control over many aspects of a networked RPi, better security, authentication, a Quick Installer, support for themes and more. All of that is mostly included in Avanto Hub.

## Avanto Hub

In Avanto Hub we have added management of network restrictions and tabs about configuring uplink url and passwords. There is also a webpage showing status of the scanned smart connectors. 

This scanned smart connectors works if the separate schub software is installed. 


 - [Hardware](#hardware)
 - [Prerequisites](#prerequisites)
 - [Quick installer](#quick-installer)
 - [Manual installation](#manual-installation)
 - [License](#license)

## Hardware
We designed this software with Raspberry Pi 3 B+ in mind and it is teseted on that. 

You need select a Raspberry Pi with suitable WLAN, Bluetooth and network backhaul. Basic devices should be capable of having one interface for backhaul (wired or wlan) and one wlan that can operate as AP. To allow smart connectors to connect there should be a bluetooth AP also. 



## Prerequisites

This software was installed on top of '2018-06-27-raspbian-stretch-lite.img' which can be downloaded from https://www.raspberrypi.org/downloads/raspbian/


## Build

Checkout sc_network from bitbucket 
tar up the files excluding .git and font-awesome (appears to have some license restrictions)
```sh
$ cd sc_network
$  tar cvf ../avantohub.tar  --exclude  .git --exclude  font-awesome ../sc_network/
```


## Quick installer
Install AvantoHub from your RaspberryPi's shell prompt. YOu can use git or the 
install tar file from above.

Transfer via ftp/usb etc the avantohub.tar file.
Untar and run the associated installer
```sh
$ tar xvf avantohub.tar -C /tmp  && bash /tmp/sc_network/installers/avanto.sh
```


The installer will complete the steps in the manual installation (below) for you.

After the reboot at the end of the installation the wireless network will be
configured as an access point as follows:
* DHCP range: 10.3.141.50 to 10.3.141.255
* SSID: `sc_hub_XXXX`
* Password: ChangeMe
* IP address: 10.3.141.1
  * Username: admin
  * Password: secret

*XXXX of sc_hub_XXXX represents the last 4 digits of CPU serial number.* It is written to hostapd.conf every boot. If you want a fixed SSID, choose a string that does not have sc_hub_ in it.

## Local customization



## Manual installation
See steps outlined in the latest release of Raspbian (currently [Stretch](https://www.raspberrypi.org/downloads/raspbian/)). 



## License
See the [LICENSE](./LICENSE) file.

