## source /tmp/raspapcommon.sh && rm -f /tmp/raspapcommon.sh
source  `dirname $0`/common.sh

function update_system_packages() {
    install_log "Updating sources"
    sudo apt-get update || install_error "Unable to update package list"
}

function install_dependencies() {
    install_log "Installing required packages"
    sudo apt-get install lighttpd $php_package hostapd dnsmasq || install_error "Unable to install dependencies"
    sudo apt-get install ppp usb-modeswitch wvdial || install_error "Unable to install packages for modem connection"
    sudo apt-get install watchdog || install_error "Unable to install packages for watchdog"
}


function download_latest_files() {
    install_log "Moving untarred files into $webroot_dir"

    if [ -d "$webroot_dir" ]; then
        sudo mv $webroot_dir "$webroot_dir.`date +%F-%R`" || install_error "Unable to remove old webroot directory"
    fi
    sudo mv  `dirname $0`/../../sc_network  $webroot_dir || install_error "Unable to move avanto hub gui files to web root"
}

# Outputs a welcome message
function display_welcome() {
    raspberry='\033[0;35m'
    green='\033[1;32m'

    echo -e "${raspberry}\n"
    echo -e "  .d888888                                     88                 " 
    echo -e " d8     88                                     88                 " 
    echo -e " 88aaaaa88a  .d       b.  .d8888b.  88aaaaa88  88888a  .d8888b.   " 
    echo -e " 88     88    88     88   88    88  88     88  88      88    88   " 
    echo -e " 88     88     88. .88    88.  .88  88     88  88      88.  .88   " 
    echo -e " 88     88        88       88888P8  88     88  88       888888    " 
    echo -e ""                                                                    
    echo -e "${green}"
    echo -e "The Quick Installer will guide you through a few easy steps\n\n"
}

install_raspap
