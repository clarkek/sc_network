#!/bin/bash
#modem-attachment.service

if [ -d /sys/class/net/wwan0 ]
then
  logger Launching wvdial, as data Modem is present
  wvdial
else
  logger No wvdial started, could not detect data Modem
  exit  1
fi
