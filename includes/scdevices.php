<?php

include_once( 'includes/status_messages.php' );

/**
*
*
*/
function DisplaySCDevices(){

  $cselected = '';
  $hselected = '';
  $tselected = '';

  /*
  * load the generated file from tmp or else load a sample one from config
  */
  $status = new StatusMessages();

  exec( 'systemctl is-active --quiet scservice', $empty, $retval);

  if( $retval != 0 ) {
    $status->addMessage('SCservice is not running' , 'warning');
  } else {
    $status->addMessage('SCservice is scanning connectors' , 'success');
  }
  if(file_exists(SCHUBSTATS)){
     $json=file_get_contents(SCHUBSTATS);
     $status->addMessage(SCHUBSTATS .' last updated at '. date ("F d Y H:i:s.", filemtime(SCHUBSTATS)) , 'info');
  } else{
  $status->addMessage(SCHUBSTATS . ' not found so showing sample results', 'warning');
     $json=file_get_contents('config/schubstats.json');
  }

  $data =  json_decode($json,true);
  ?>
  <div class="row">
  <div class="col-lg-12">
  <div class="panel panel-primary">
  <div class="panel-heading"><i class="fa fa-plug fa-fw"></i> <?php echo _("List of Smart Connectors"); ?></div>
  <div class="panel-body">

   <p><?php $status->showMessages(); ?></p>

  <head>
  <meta http-equiv="refresh" content="10" />
  </head>
  <?php
  echo '<table style="width:100%">' ;
    if (count($data)) {
        $deviceids= array_keys  ($data)  ;
        $deviceid = $deviceids[0];
        $datakeys = array_keys ($data["$deviceid"] );

        echo "<tr>\n" ;
        echo "<td>  DeviceAddress  </td>";
        foreach ($datakeys as $index => $keyname) {
                echo "<td>  $keyname  </td>";
        }
        echo "</tr>\n" ;
        foreach ($deviceids as $deviceid) {
            echo "<tr><td> $deviceid" ;
            foreach ($datakeys as $datakey) {
                $wdata = $data["$deviceid"]["$datakey"] ;
                $wdata = var_export($wdata,true) ;
                echo "<td>  $wdata  </td>";  }
            echo "</tr>\n" ;
        }
        echo '</tr>';
    }
  echo '</table>';
  ?>
    </div><!-- /.panel-body -->
    </div><!-- /.panel-default -->
    </div><!-- /.col-md-6 -->
    </div><!-- /.row -->

    <form action="?page=system_info" method="POST">
      <input type="button" class="btn btn-outline btn-primary" value="<?php echo _("Refresh"); ?>" onclick="document.location.reload(true)" />
    </form>

  </div><!-- /.panel-body -->
  </div><!-- /.panel-primary -->
  </div><!-- /.col-lg-12 -->
  </div><!-- /.row -->
  <?php
}

